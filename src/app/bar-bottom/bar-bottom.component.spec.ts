import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BarBottomComponent } from './bar-bottom.component';

describe('BarBottomComponent', () => {
  let component: BarBottomComponent;
  let fixture: ComponentFixture<BarBottomComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BarBottomComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BarBottomComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
