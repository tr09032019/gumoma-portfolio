import { Component, OnInit, Renderer2 } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';

// LE IMPORTO EL SERVICIO
import { ActivatedRoute } from '@angular/router';
import { BackendService } from '../../backend.service';
import { Location } from '@angular/common';



@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.scss']
})

export class DetailsComponent implements OnInit {

  slug : string;
  workDetail : any[] = [];

  constructor(private renderer: Renderer2, private _sanitizer: DomSanitizer, private activatedRoute: ActivatedRoute, private workService: BackendService, private location: Location) {

    this.activatedRoute.params.subscribe(params => {
      this.slug = params['slug'];

      this.workService.getWorkId(this.slug).subscribe(datos => {
        this.workDetail = datos['works'];
      })
    })

  }

  ngOnInit() {
    this.workSingle()
  }

  public sanitizeImage(image: string) {
    return this._sanitizer.bypassSecurityTrustStyle(`url(${image})`);
  }

  workSingle() {
    this.workService.getWorks().subscribe(
      datos => this.workDetail = datos['works']
    )
  }

  backToWorks() {
    this.location.back()
  }

  showImage(url : string) {
    let modal = document.querySelector('#modal');
    let imgmodal = document.querySelector('#imgmodal');
    modal.classList.remove('none');
    
    this._sanitizer.bypassSecurityTrustStyle(`url(${url})`);
    imgmodal.setAttribute('src', `${url}`);
  }

  closeModal() {
    let modal = document.querySelector('#modal');
    let imgmodal = document.querySelector('#imgmodal');
    let close = document.querySelector('#close');
    close.addEventListener('click', e => {
      modal.classList.add('none');
      imgmodal.setAttribute('src', '');
    })
  }

}
