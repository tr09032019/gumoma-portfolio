import { Component, OnInit } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';

// IMPORTO EL SERVICIO
import { BackendService } from '../backend.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-works',
  templateUrl: './works.component.html',
  styleUrls: ['./works.component.scss']
})

export class WorksComponent implements OnInit {

  works : any[] = []

  constructor(private _sanitizer: DomSanitizer, private backendService: BackendService, private router: Router) { }

  ngOnInit() {
    this.worksgu()
  }

  public sanitizeImage(image: string) {
    return this._sanitizer.bypassSecurityTrustStyle(`url(${image})`);
  }

  worksgu() {
    this.backendService.getWorks().subscribe(
      datos => this.works = datos['works']
  )}

  goWork(slug: string) {
    this.router.navigate(['detalles', slug])
  }

}
