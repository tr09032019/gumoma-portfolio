import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PageOnComponent } from './page-on.component';

describe('PageOnComponent', () => {
  let component: PageOnComponent;
  let fixture: ComponentFixture<PageOnComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PageOnComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PageOnComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
