import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';

@Component({
  selector: 'app-page-on',
  templateUrl: './page-on.component.html',
  styleUrls: ['./page-on.component.scss']
})
export class PageOnComponent implements OnInit {
  
  constructor(private _router: Router, private miruta: ActivatedRoute) { 
    this.miruta.queryParams.subscribe((params: Params) => {
      console.log(params);
    });
  }

    ngOnInit() {
  }

}
