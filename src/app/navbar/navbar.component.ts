import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {


  constructor() { }

  ngOnInit() {
    this.event()
  }

  
  estado : number = 0;
  event() {

    let burguer = document.querySelector('#burguer');
    let menu = document.querySelector('#menu');
    let home = document.querySelector('#home');
    let works = document.querySelector('#works');
    let aboutme = document.querySelector('#aboutme');
    let contact = document.querySelector('#contact');

    
    burguer.addEventListener('click', e => {
      if (this.estado === 0) {
        this.estado = 1;
        console.log(this.estado);
        menu.classList.remove('transform');
        // menu.classList.remove('none');
      } else {
        this.estado = 0;
        console.log(this.estado);
        menu.classList.add('transform');
        // menu.classList.add('none');
      }
    })

    home.addEventListener('click', e => {
      this.estado = 0;
      menu.classList.add('transform');
      menu.classList.add('none');
    })

    works.addEventListener('click', e => {
      this.estado = 0;
      menu.classList.add('transform');
      menu.classList.add('none');
    })

    aboutme.addEventListener('click', e => {
      this.estado = 0;
      menu.classList.add('transform');
      menu.classList.add('none');
    })

    contact.addEventListener('click', e => {
      this.estado = 0;
      menu.classList.add('transform');
      menu.classList.add('none');
    })
  }
  

  
  

  

  }
