import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HomeComponent } from './home/home.component';
import { AboutMeComponent } from './about-me/about-me.component';
import { ContactoComponent } from './contacto/contacto.component';
import { WorksComponent } from './works/works.component';
import { DetailsComponent } from './works/details/details.component';



const routes: Routes = [
  { path: 'inicio', component: HomeComponent },
  { path: 'trabajos', component: WorksComponent },
  { path: 'detalles/:slug', component: DetailsComponent },
  { path: 'sobre-mi', component: AboutMeComponent },
  { path: 'contacto', component: ContactoComponent },
  { path: '**', pathMatch: 'full', redirectTo: 'inicio' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
