import { Injectable } from '@angular/core';

// IMPORTO LIBRERÍA PARA HACER PETICIONES
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})


export class BackendService {

  url : string = '../assets/back/works.json';

  constructor(private http: HttpClient) { }

      // MÉTODO DE MI SERVICIO

      getWorks() {
        return this.http.get<any[]>(this.url);
      }

      getWorkId(workId) {
        return this.http.get<any[]>(this.url);
      }

}
